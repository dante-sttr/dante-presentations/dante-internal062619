<!doctype html>
<html>
	<head>
		<meta charset="utf-8">

		<title>reveal.js – The HTML Presentation Framework</title>

		<meta name="description" content="DANTE Project Internal Workshop">
		<meta name="author" content="Joshua S Brinks">

		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

		<title>DANTE Project Internal Workshop</title>

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/myplanet.css">
		<link rel="stylesheet" href="css/presentation.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="lib/css/monokai.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
	</head>
	<body>
		<div class="reveal pattern--random">
			<div class="slides">
				<!-- Title Slide -->
				<section>
					<h1>DANTE Project Internal Workshop</h1>
					<hr style='background-color:#000000;border-width:0;color:#000000;height:5px;'>
					<h3>26 June, 2019</h3>
					<h3>Interchurch Building, Columbia University</h3>
					<img class="plain" src="data_types.png" width=75%>
					<p>
						<small>Joshua Brinks and DANTE Project Contributors</small>
					</p>
				</section>
				
				
				<!-- Administrative and Project -->
				<section>
				  
				<section>
				<h1>General Administrative Review </h1>
				<h3>10:00am - 10:30am</h3>
				</section>  
				
				<!-- Overview and Mission Statement -->  
				<section>
				<h1>Project Overview</h1>
				
				<p>Mission Statement: Accelerate progress of environment-security analysis by developing an open-source community platform with a suite tools that reduce barriers to entry.</p>
				</section>
				
				<section>
				  <h1>Project Components</h1>
				<p>These goals will be achieved by facilitating several development tasks:</p>
				<ul>
				  <li>Populate and currate the DANTE Citation Repository</li>
				  <li>Incorporate datasets into the DANTE Dataset Catalogue</li>
				  <li>Develop a variety of instructional manuals, vignettes, and scientific manuscripts tailored to the climate-security sector.</li>
				  <li>Git repositories for documentation, training materials, and developer interactions.</li>
				  <li>User engagement and outreach.</li>
				</ul>
				  
				</section>
				
				<section>
				<h1>Development Phases: The Spirals</h1>
				<p>Project rollout is organized into 3 phases we describing as "Spirals".</p>
				<p>Each spiral has a thematic focus with a collection of targeted datasets, tool development, and vignettes.</p>
				
				</section>
				
				<section>
				<h2>Spiral One</h2>
				  
				</section>
				
				<section>
				<h2>Spiral Two</h2>
				  
				</section>
				
				<section>
				<h2>Spiral Three</h2>
				  
				</section>
				
				</section>
				
				<!-- Public Outreach & Communications -->
				<section>
				  <section>
				    <h1>Current and Potential Outreach Activities</h1>
				    <h3>10:30am - 11:15am</h3>
				  </section>
				  
				  <section>
				    <h2>Climate Change, Human Migration and Health</h2>
				    <ul>
				      <li>Significantly proportion of attendees were early career researchers and graduate students.</li>
				      <li>Interest on the possibilities that different tools offer for data discovery, not only processing.</li>
				      <li>Interest in any toolkit that facilitate interacting with remote sensing data.</li>
				    </ul>
				  </section>
				  
				   <section>
				    <h2>UN High-Level Experts and Leaders Panel on Water and Disasters (HELP)</h2>
				    <ul>
				      <li>Monday, June 24</li>
				      <ul>
				        <li>Ministerial level meeting at UN HQ</li>
				        <li>Lieutenant General Todd T. Semonite will attend/speak</li>
				      </ul>
				      <li>Tuesday, June 25</li>
				      <ul>
				        <li>Panel meeting, likely at Ford Foundation Center for Social Justice</li>
				        <li>Lieutenant General Todd T. Semonite will attend/speak</li>
				        <li>Morning will be ministerial level</li>
				        <li>Two afternoon breakout sessions. Possible speaking opportunity</li>
				        <li>Reception that evening hosted by Columbia University/CIESIN</li>
				      </ul>
				    </ul>
				  </section>
				  
				   <section>
				    <h2>American Geophysical Union Fall Metting</h2>
				  </section>
				  
				  <section>
				    <h2>100th Meeting of the American Meteorological Society</h2>
				    <ul>
				      <li>Contacted AMS Meetings staff and Centennial Committee leadership who expressed interest.</li>
				      <li>Consulted with Stephanie Herring, Chair of the AMS Board on Global Strategies and confirmed interest in 
				      co-sponsoring the Town Hall and engaging their Environment and Security Committee.</li>
				    </ul>
				  </section>
				  
				  <section>
				    <h2>Additional Outreach Activities</h2>
				    <ul>
				      <li>Continued regular monitoring of additional potential opportunities to highlight DANTE in professional
				      society and other meetings in 2019 and 2020.</li>
				      <li>Made referral from contacts Stephan Harrison and Tessa Kingsley, of UK firm Climate Change Risk Management,
				      who work on the development of climate projections for climate resilience and risk management, in a number of
				      countries on this</li>
				    </ul>
				  </section>
				  
				</section>
			
			  <!-- Technical Progress -->
			  <section>
			    <h1>Technical Progress</h1>
			    <h3>11:15am - 12:00pm</h3>
			  </section>
			  
			  <!-- Web Design -->  
  			<section>
  			  <section>
  			    <h2>Web Design and Mockups</h2>
  			    Developing a variety of logos
  			  </section>
  			</section>
			
  			<!-- Package Development --> 
  			<section>
  			  <section>
  			    <h2>Git Repository & Package Development</h2>
  			    Our GitLab DANTE Project repository has been established.
  			    All packages can be installed from GitLab. 
  			    We have established Continuous Integration for R packages. This ensures packages pass quality checks similar to CRAN. 
  			  </section>
  			  
  			  <!-- danteSubmit --> 
  			  <section>
  			    <img src="figures/dantesubmit_logo.svg" style="width: 175px; float:right; background:none; border:none;">
  			    <h2 style="width:50%; align:top;"><code>danteSubmit</code></h2>
  			    
  			   <p><code>danteSubmit</code> serves as centralized distribution for DANTE content submissions. This includes:</p>
  			  
  			    <ul>
  			      <li>Functions and packages</li>
  			      <li>DANTE dataset pages</li>
  			      <li>DANTE vignettes</li>
  			    </ul>
  			    
  			    
  			     </section>
  			
    			<section>
    			  <h2><code>danteSubmit</code></h2>
    			  <code>danteSubmit</code> provides GitLab hosted tutorials and walkthroughs for generating DANTE Project content. <br/><br/>
    			  
    			    <iframe src="https://dante-sttr.gitlab.io/dantesubmit/index.html" style='width: 700px; height: 450px; margin-left: 110px;'></iframe>
    			</section>    
  			    
  			 
  			  <!-- untools --> 
  			   <section>
  			    <h2><code>untools</code></h2>
  			    <p>The <code>untools</code> package provides support for United Nations datasets. Currently we offer functionality for: </p>
  			    
  			    <ul>
  			      <li>High Commissioner on Refugees</li>
  			      <li>Migrant Stock Populations (2017 Revision)</li>
  			    </ul>
  			  </section>
  			  
  			  <section>
  			    <h2><code>untools</code></h2>
  			    <p>All our packages are hosted with embedded vignettes illustrating function use and package capabilities.</p>
  			        <iframe src="https://dante-sttr.gitlab.io/untools/index.html" style='width: 700px; height: 450px; margin-left: 110px;'></iframe>
    			    </section>    
    			    
    			    <!-- conflictr --> 
  			   <section>
  			    <h2><code>conflictr</code></h2>
  			    <p>The <code>conflictr</code> package provides support for widely used datasets depicting international conflict. Currently we offer functionality for: </p>
  			    
  			    <ul>
  			      <li>The Armed Conflict Location & Event Data Project (ACLED)</li>
  			      <li>Uppsala Conflict Data Program (UCDP)</li>
  			      <li>Mass Episodes of Political Violence (MEPV)</li>
  			    </ul>
  			  </section>
  			  
  			  <section>
  			    <h2><code>untools</code></h2>
  			    <p>All our packages are hosted with embedded vignettes illustrating function use and package capabilities.</p>
  			        <iframe src="https://dante-sttr.gitlab.io/conflictr/index.html" style='width: 700px; height: 450px; margin-left: 110px;'></iframe>
    			    </section>    
  		
  		<!-- commonCodes --> 
  			   <section>
  			    <h2><code>commonCodes</code></h2>
  			    <p>The <code>commonCodes</code> package provides country code and identification harmonization support for several widely used climate-security datasets: </p>
  			    
  			    <ul>
  			      <li>Global Administrative Boundaries (GADM)</li>
  			      <li>The <code>raster</code> package’s country codes dataset</li>
  			      <li>United Nations High Commissioner for Refugees Populations of Concern (Time Series)</li>
  			      <li>United Nations Migrant Stock Populations (2017 Revision)</li>
  			      <li>Mass Episodes of Political Violence (MEPV)</li>
  			      <li>International Organization for Standardization (ISO)</li>
  			      <li>The Correlates of War Project (CoW)</li>
  			      <li>Gleditsch and Ward country codes </li>
  			      <li>International Monetary Fund country codes (IMF)</li>
  			    </ul>
  			  </section>
  			  
  			  <section>
  			    <h2><code>commonCodes</code></h2>
  			    <p>All our packages are hosted with embedded vignettes illustrating function use and package capabilities.</p>
  			        <iframe src="https://dante-sttr.gitlab.io/commoncodes/index.html" style='width: 700px; height: 450px; margin-left: 110px;'></iframe>
    			    </section>    
  			  
  			</section>
  			
  			<!-- Dataset Pages --> 
  			<section>
  			<section>
  			  <h1>DANTE Dataset Pages</h1>
  			</section>
			  
			  	<section>
  			  <p>In addition to standard information describing dataset authors, hosting information, and spatial and temporal extents, we present a variety of supplementary tools and information:</p>
  			  <ul>
  			    <li>Discusion points on its use in the research and practitioner communities.</li>
  			    <li>Critical commentary of where the dataset excells.</li>
  			    <li>Data’s potential biases, drawbacks, or other methodological flaws</li>
  			    <li>R packages or other software developed specifically to compliment the dataset.</li>
  			    <li>Vignettes and tutorials utilizing the data.</li>
  			    <li>Direct commentary from DANTE users.</li>
  			  </ul>
  			</section>
  			
  				<section>
  			  <p>Completed dataset pages for the following datasets:</p>
  			  <ul>
  			    <li>ACLED</li>
  			    <li>UCDP-GED</li>
  			    <li>UNHCR Persons of Concern Time Series</li>
  			    <li>UN Migrant Stocks (2017 Revision)</li>
  			    <li>Standardized Precipitation Evapotranspiration Index</li>
  			    <li>WSIM (GLDAS 2.0)</li>
  			    <li>MEPV</li>
  			  </ul>
  			</section>
  			
  			<section>
  			  <h1>Sample Dataset Pages</h1>
  			  
  			  <iframe src="https://dante-packages.s3.us-east-2.amazonaws.com/unstocks17_unstocks17.html" style='width: 49%; height: 450px; float:left'></iframe>
  			  
  			  <iframe src="https://dante-packages.s3.us-east-2.amazonaws.com/spei_spei.html" style='width: 49%; height: 450px; float:right'></iframe>
  			</section>
  			
  			</section>
			
					<!-- Lunch -->
    			<section>
    			  
    			  <section>
    			    <h1>Break For Lunch</h1>
    			    <h3>12:00pm - 1:00pm</h3>
    			  </section>
    			  
    			</section>
			
			</div>
		</div>

		<script src="js/reveal.js"></script>

		<script>
			// More info about config & dependencies:
			// - https://github.com/hakimel/reveal.js#configuration
			// - https://github.com/hakimel/reveal.js#dependencies
			Reveal.initialize({
			  // Display a presentation progress bar
	progress: true,
					dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/highlight/highlight.js', async: true },
					{ src: 'plugin/zoom-js/zoom.js'}
				]
			});
		</script>
	</body>
</html>
